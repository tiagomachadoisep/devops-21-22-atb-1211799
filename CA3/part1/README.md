# Class Assignment 2 - Part 1 - Build Tools with Gradle

## 1.1 Analysis, Design and Implementation

### Requirements:

* ####  Create a VM as described in the lecture;

This first step was fairly simple. Virtual box was available on ubuntu store, so I downloaded it fromt there. After that I just followed along the professor's lecture to create our first VM.

* #### Clone individual repository inside the VM;

This step was done using the git clone command. I want to point out that after failing to troubleshoot why my chat app was not running properly I decided to just clone my colleague repository instead.

* #### Build and Execute the spring boot projects from the previous assignments;

After cloning the individual repository I cloned the spring boot tutorial, like I did in the previous assignment. Knowing that I would eventually need to use maven and gradle I installed them aswell:

> sudo apt install maven
> sudo apt install gradle

Having done that I tried to run the app:

> ./mvnw spring-boot:run

Since I followed along the teacher's lecture, I had the same IP address for my virtual machine. So, to check if the app was online I just went to the following URL: http://192.168.56.5:8080/; and the result was the following:

![](img/1-1.png)

For CA2 assignment, I tried to run the app just like in the previous assignment, with the grdle build command, but the permission was denied, so I gave myself permission to execute the app:

>  chmod u+x gradlew

After this I was able to execute tasks with gradle, so I executed the runServer task.
The build failed and I got an error saying that I needed a display, which obviously wasn't available for the vm. So I created a new task to solve this issue:

>task runClientVM(type:JavaExec, dependsOn: classes){
>
>    group = "DevOps"
>    description = "Launches a chat client that connects to a server on 192.168.56.5 "
>
>    classpath = sourceSets.main.runtimeClasspath
>
>    main = 'basic_demo.ChatClientApp'
>
>        args '192.168.56.5', '59001'
>    }


And the build was successful.


