# Class Assignment 2 - Part 1 - Build Tools with Gradle

## 1.1 Analysis, Design and Implementation

### Requirements:



* #### Use Vagrant to setup a virtual environment;

I downloaded Vagrant via the command line, to my ubuntu.
Having Vagrant installed I followed along with the teacher example:

Firstly I created a directory to initiate vagrant.
After that in the newly created directory I ran this command:

> vagrant init bento/ubuntu-18.04


and then this command:

> vagrant box add bento/ubuntu-18.04

and finally:


> vagrant up

* #### Copy Vagrantfile to repository;

This task was done just by committing the recently created vagrant file.

* #### Update Vagrantfile configuration

For this step I just added correct repository in the line that the teacher told us to.


* #### Replicate the changes in my version of the spring application so it uses the H2 server in the VM;

Having made the changes to the spring app I ran vagrant up and checked that the applications were running in right URL's as shown bellow:
![](img/part3.2-1.png)
![](img/part3.2-2.png)
![](img/part3.2-3.2.png)
![](img/part3.2-3web.png)
![](img/part3.2-4.png)

## 2 Analysis of an Alternative

As an alternative I chose to analyse and work with VMware, for its extended online support and better troubleshooting if needed.

The following table provides a good understanding of the key differences between virtual box and VMware

|                          | VirtualBox                              | VMware                                                                                                                                                                                     |
|--------------------------|-----------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Hardware and software virtualisation | Provides virtualization for both hardware and software | Provides virtualization only for hardware.                                                                                                                                                 |
| Host operating system    | VirtualBox is available for Linux, Windows, Solaris, macOS, FreeBSD etc. There exists a wide scope of supporting various OS. | VMare is available for Linux and Windows. You must install VMware Fusion/Fusion Pro if you have a Mac. There is a narrow scope of supporting various OS.                                   |
| Guest operating system   | On VirtualBox-based VMs, the following guest operating systems can be installed: Linux, Windows, Solaris, FreeBSD, and macOS. | VMware also supports operating systems such as Windows, Linux, Solaris, and Mac. The only distinction is that to operate macOS virtual machines, VMware requires VMware Fusion/Fusion Pro. |
|         Virtual disk format | VirtualBox supports  VDI (Virtual Disk Image),  Virtual Machine Disk (VMDK),   Virtual Hard Disk (VHD). | VMware support Virtual Machine (VMDK)                                                                                                                                                      |
|            User-friendly interface              |         virtualBox provides a user-friendly interface.| VMware provides a complicated user interface.                                                                                                                                                   |
|         USB support                 |     The Extension Pack is required for USB 2.0/3.0 functionality (free) incase of VirtualBox.                                    |VMware provides out of the box USB device support.
|           Support for 3D graphics               |     It can only support 3D graphics Up to OpenGL 3.0 and Direct3D 9.                                    |VMware provides 3D graphics with DirectX 10 and OpenGL 3.3 support for all of its products.
|          Video memory                | Video memory, incase of VirtualBox, is limited to 128 MB.                                        |Video memory, incase of VMware, is limited to 2 GB.
|         Hypervisor                 |    ·VirtualBox is a Type 2 Hypervisor. A hypervisor creates the virtualization layer that separates the guest machine from the underlying operating system.                                     |VMware ESXi is a Type 1 Hypervisor that works directly on the host machine’s hardware resources. Some VMware products, such as VMware Player, VMware Workstation, and VMware Fusion, are Type 2 Hypervisors.|



## 3. Implementation of the Alternative
 To star with this task I downloaded VMware workstation version from this [site](https://www.vmware.com/latam/products/workstation-pro/workstation-pro-evaluation.html). After that I installed the necessary plugin as suggested in the Vagrant [website](https://www.vagrantup.com/docs/providers/vmware/installation) using the following command:

> vagrant plugin install vagrant-vmware-desktop

And then I copied the vagrant file of this assignment and changed the line where we choose our provider from virtual box to vmware workstation. And when I ran Vagrant up everything was working fine!
![](img/alt-1.png)
![](img/alt-3.png)
![](img/alt-4.png)