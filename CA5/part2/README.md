# Class Assignment 5 - CI/CD Pipelines with Jenkins

## Part 2

## 1.1 Analysis, Design and Implementation

For this assignment we had 2 more stages than in the CA5-part1 assignment, namely:
* Javadoc: Generates the javadoc of the project and publish it in Jenkins;
* Publish Image: Generate a docker image with Tomcat and the war file and publish it
in the Docker Hub;

For the Javadoc stage I added the following code to the new Jenkinsfile:

````
stage('Javadocking') {
            steps {
                echo 'Javadocking ...'
                dir('CA2/Part2/tut-basic-gradle') {
                    sh './gradlew javadoc'
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'build/docs/javadoc', reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: ''])
                }
            }
        }
````

Applying the Java plugin to a Gradle build script automatically provides a task for generating Javadocs. That task is called javadoc. It takes the compiled source code from the main source set as input and builds a set of HTML pages as output. For the next part of this stage I installed the *HTML Publisher Plugin*, this plugin is used to publish HTML reports that our builds generates to the job and build pages. 
Next up, I used the *Snippet Generator* feature on Jenkins to generate a a snippet that allows us to publish our HTML reports in the build page, as seen bellow:

![](img/javadoc-report.png)

Finally, for the Publish Image stage, I added this code:

````
stage('Docker Image') {
            steps {
                echo 'Publishing Docker Image...'
                dir('CA2/Part2/tut-basic-gradle') {
                    script {
                        docker.withRegistry('https://registry.hub.docker.com/', 'DockerHubCredentials') {
                            def customImage = docker.build("tiago1211799/test_repo:${env.BUILD_ID}")

                            customImage.push()
                        }
                    }
                }
            }
        }
````

For this stage the following documentation ([https://www.jenkins.io/doc/book/pipeline/docker/](https://www.jenkins.io/doc/book/pipeline/docker/)) proved to be highly valuable, as it had several pipelines examples that had a lot of good information.

So, firstly we need to install the *Docker Pipeline* plugin, this plugin allows us to use several useful methods related to docker. The *withRegistry* function accepts 2 arguments, the first is mandatory and specifies the registry url, the second is optional and provides the credentials to log in.
This credentials were created using the *Manage Credentials* feature in Jenkins.

We then build the image with the tag build function. We just needed to indicate the tag name that we want, in my case I used the build number to avoid overwriting images.

Lastly we push the image to our hub, with the push() function.

As we can see the build was successful and the tag was pushed to my docker hub:

![](img/success-build.png)

![](img/tag-in-docker-hub.png)



## 2 Analysis of an Alternative - Buddy

Buddy, as some of the alternatives analyzed in previous CA's, comes as a different tool, with different use cases. 
Firstly, Jenkins, from what I learned in my research, has been the go-to solution for CI/CD for some years now, and is well established, as Buddy is a younger solution.
The first noticeable difference is that Jenkins comes is available as a package and need an environment to run, while Buddy, being a SaaS, is accessed via the internet.
Another obvious difference, is that Buddy's GUI is a lot more user and beginner friendly than Jenkins.
When it comes to Integrations, Jenkins being open-source, relies on plugins developed by the open-source community that need to be installed, while Buddy comes with pre-installed plugins, developed by Buddy technical team, that assures the plugins don't interfere with one another.
Creating pipelines is also very different, in Jenkins, as we have seen before, we can use either declarative or imperative pipelines, that are pipelines as code in the form of the Jenkinsfile. With Buddy we can either use the GUI or we can write a *buddy.yaml* file to run our script.

The following board summarizes the main differences:

![](img/buddy-vs-jenkins.png)


## 3. Implementation of the Alternative 

First of all,, to use buddy we need to sign up, log in, and verify our email, in order to be able to use it. Having done so, we can start by selecting the *Create Project* button, this will take us to an interface where we can choose from all the repositories associated with the account that we logged on to.
I created a new repository with the CA2-part2 content to facilitate this assignment. 

Having done this we can start by clicking in the *Add new pipeline* button, where we can name our pipeline and manage the general settings. When we proceed we prompted to add a new *action* to our pipeline.

In my first action I tried to replicate the stages that were developed for Jenkins, and so, after some learning by mistakes, I came to the following action:

![](img/first-action.png)

As we can see, we just execute the gradle tasks that were executed in the Jenkins stages.

The javadoc files were generated successfully:

![](img/javadoc.png)

The test reports were also successfully generated:

![](img/tests-reports.png)

And the app was successfully packaged in a war file:

![](img/archive.png)


Next up, we add a new action, named "Build Docker image":

![](img/build-docker-image.png)


Afterwards we can select the desired Dockerfile path, that the action is gonna build from, as we can see bellow:

![](img/select-dockerfile.png)



Lastly we just need another action responsible to push the image to our repo. We select a new action named "Push Docker image", where, just like in Jenkins, we need to indicate our docker hub credentials, and desired tag, and like in Jenkins, I added the build number as a variable to avoid overlapping images:

![](img/docker-push-action.png)

As we can see the image was successfully pushed:

![](img/docker-pushed.png)

The pipeline was able to operate just fine:

![](img/successful_run.png)








