package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void shouldThrowANullPointerExceptionWithNullFirstName() {
        String validLastName = "Bar";
        String validDescription = "Code Tester";
        String validJobTitle = "Tester";
        int validJobYear = 2;
        String validEmail = "valid@gmail.com";

        assertThrows(NullPointerException.class,
                () -> {
                    new Employee(null, validLastName, validDescription, validJobTitle, validJobYear, validEmail);
                });
    }

    @Test
    void shouldThrowANullPointerExceptionWithNullLastName() {
        String validFirstName = "Foo";
        String validDescription = "Code Tester";
        String validJobTitle = "Tester";
        int validJobYear = 2;
        String validEmail = "valid@gmail.com";


        assertThrows(NullPointerException.class,
                () -> {
                    new Employee(validFirstName, null, validDescription, validJobTitle, validJobYear, validEmail);
                });
    }

    @Test
    void shouldThrowANullPointerExceptionWithNullDescription() {
        String validFirstName = "Foo";
        String validLastName = "Bar";
        String validJobTitle = "Tester";
        int validJobYear = 2;
        String validEmail = "valid@gmail.com";


        assertThrows(NullPointerException.class,
                () -> {
                    new Employee(validFirstName, validLastName, null, validJobTitle, validJobYear, validEmail);
                });
    }

    @Test
    void shouldThrowANullPointerExceptionWithNullJobTitle() {
        String validFirstName = "Foo";
        String validLastName = "Bar";
        String validDescription = "Code Tester";
        int validJobYear = 2;
        String validEmail = "valid@gmail.com";

        assertThrows(NullPointerException.class,
                () -> {
                    new Employee(validFirstName, validLastName, validDescription, null, validJobYear, validEmail);
                });
    }

    @Test
    void shouldThrowAnIllegalArgumentExceptionWithEmptyFirstName() {
        String validLastName = "Bar";
        String validDescription = "Code Tester";
        String validJobTitle = "Tester";
        int validJobYear = 2;
        String validEmail = "valid@gmail.com";

        assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee("", validLastName, validDescription, validJobTitle, validJobYear, validEmail);
                });
    }

    @Test
    void shouldThrowAnIllegalArgumentExceptionWithEmptyLastName() {
        String validFirstName = "Foo";
        String validDescription = "Code Tester";
        String validJobTitle = "Tester";
        int validJobYear = 2;
        String validEmail = "valid@gmail.com";

        assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(validFirstName, "", validDescription, validJobTitle, validJobYear, validEmail);
                });
    }

    @Test
    void shouldThrowAnIllegalArgumentExceptionWithEmptyDescription() {
        String validFirstName = "Foo";
        String validLastName = "Bar";
        String validJobTitle = "Tester";
        int validJobYear = 2;
        String validEmail = "valid@gmail.com";

        assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(validFirstName, validLastName, "", validJobTitle, validJobYear, validEmail);
                });
    }

    @Test
    void shouldThrowAnIllegalArgumentExceptionWithEmptyJobTitle() {
        String validFirstName = "Foo";
        String validLastName = "Bar";
        String validDescription = "Code Tester";
        int validJobYear = 2;
        String validEmail = "valid@gmail.com";

        assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(validFirstName, validLastName, validDescription, "", validJobYear, validEmail);
                });
    }


    @Test
    void shouldThrowAnIllegalArgumentExceptionWithNegativeJobYear() {
        String validFirstName = "Foo";
        String validLastName = "Bar";
        String validDescription = "Code Tester";
        int negativeJobYear = -2;
        String validEmail = "valid@gmail.com";

        assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(validFirstName, validLastName, validDescription, "null", negativeJobYear, validEmail);
                });
    }

    @Test
    void shouldThrowAnIllegalArgumentExceptionWithEmptyEmail() {
        String validFirstName = "Foo";
        String validLastName = "Bar";
        String validDescription = "Code Tester";
        int negativeJobYear = -2;
        String emptyEmail = "";

        assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(validFirstName, validLastName, validDescription, "null", negativeJobYear, emptyEmail);
                });
    }

    @Test
    void shouldThrowANullArgumentExceptionWithNullEmail() {
        String validFirstName = "Foo";
        String validLastName = "Bar";
        String validDescription = "Code Tester";
        int negativeJobYear = -2;

        assertThrows(NullPointerException.class,
                () -> {
                    new Employee(validFirstName, validLastName, validDescription, "null", negativeJobYear, null);
                });
    }

    @Test
    void shouldThrowAnIllegalArgumentExceptionWhenEmailDoesNotHaveEmailSign() {
        String validFirstName = "Foo";
        String validLastName = "Bar";
        String validDescription = "Code Tester";
        int negativeJobYear = -2;
        String invalidEmail = "fsdkjhfkdsfhj.pt";

        assertThrows(IllegalArgumentException.class,
                () -> {
                    new Employee(validFirstName, validLastName, validDescription, "null", negativeJobYear, invalidEmail);
                });
    }
}