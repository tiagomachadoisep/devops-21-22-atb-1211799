# Class Assignment 2 - Part 1 - Build Tools with Gradle

## 1.1 Analysis, Design and Implementation

### Requirements:

* #### You should start by downloading and commit to your repository (in a folder for Part1 of CA2) the example application.

Firstly I started off by creating all the folders needed for this assignment. Having done that, I needed to head to the
repository, download it and add it to the newly created folder. Added the changes to the staging area and commit them.

* #### Read the instructions available in the readme.md file and experiment with the application.

The demo project has a rather simple README file, and therefore, following it along was easy. First I checked if I had
all the prerequisites in order, then ran all the commands and "played" a little with the chat. It was the first time
that I interacted with a java GUI, even tho, it was a very simple one, it was good.

* #### Add a new task to execute the server.

New tasks, in gradle, can be added in the build.gradle file, within a gradle project. To run these tasks we just need to
type:

> ./gradlew *taskName*

Having said that, I added a new task to execute the server named *runServer* to the build.gradle file, with the
following code:

> task runServer(type:JavaExec, dependsOn: classes) {  
> &nbsp;&nbsp;&nbsp;&nbsp; group = "DevOps"  
> &nbsp;&nbsp;&nbsp;&nbsp; description = "Launches a chat client that connects to a server on localhost:8080 "  
> &nbsp;&nbsp;&nbsp;&nbsp; classpath = sourceSets.main.runtimeClasspath       
> &nbsp;&nbsp;&nbsp;&nbsp; mainClass = 'basic_demo.ChatClientApp'         
> &nbsp;&nbsp;&nbsp;&nbsp; args 'localhost', '59001'  
> }

* #### Add a simple unit test and update the gradle script so that it is able to execute the test.

Adding a test to this project could be done in multiple ways. First I tried to add the dependencies in build.gradle
file, like so:

> dependencies {  
> &nbsp;&nbsp;&nbsp;&nbsp; // Use Apache Log4J for logging  
> &nbsp;&nbsp;&nbsp;&nbsp; implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'  
> &nbsp;&nbsp;&nbsp;&nbsp; implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'  
> &nbsp;&nbsp;&nbsp;&nbsp; **testImplementation 'org.junit.jupiter:junit-jupiter-api:4.12'**  
> &nbsp;&nbsp;&nbsp;&nbsp; **testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:4.12'**  
> }

Then I opened the *App.java* file and used the Intellij *generate test* function to create a new test file.
I chose the *JUnit 4* instead of *JUnit 5* like advised in our paper. 
Then pasted the test code provided by the teacher and ran the test. The test passed and everything ran smoothly.
When I checked the build.gradle file I noticed a new dependency was added.
The dependencies were like this:

> dependencies {  
> &nbsp;&nbsp;&nbsp;&nbsp; // Use Apache Log4J for logging  
> &nbsp;&nbsp;&nbsp;&nbsp; implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'  
> &nbsp;&nbsp;&nbsp;&nbsp; implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'  
> &nbsp;&nbsp;&nbsp;&nbsp; **implementation 'junit:junit:4.12'**  
> &nbsp;&nbsp;&nbsp;&nbsp; testImplementation 'org.junit.jupiter:junit-jupiter-api:4.12'   
> &nbsp;&nbsp;&nbsp;&nbsp; testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:4.12'  
> }

I deleted the first dependencies I added to check if the test still ran, and they did, so I deleted them, leaving the dependencies like they are now:

> dependencies {  
> &nbsp;&nbsp;&nbsp;&nbsp; // Use Apache Log4J for logging  
> &nbsp;&nbsp;&nbsp;&nbsp; implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'  
> &nbsp;&nbsp;&nbsp;&nbsp; implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'  
> &nbsp;&nbsp;&nbsp;&nbsp; implementation 'junit:junit:4.12'  
> }

* #### Add a new task of type Copy to be used to make a backup of the sources of the application.

In [this](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.Copy.html) documentation page we can see that there is a task type *Copy* that does exactly what we need,
so following the documentation page I wrote the following task:

> task backup(type: Copy){  
> &nbsp;&nbsp;&nbsp;&nbsp; from 'src'  
> &nbsp;&nbsp;&nbsp;&nbsp; into 'backup'  
> }

And confirmed that a new folder named *backup* was created, mirroring the content of the source folder, like intended.

* #### Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application.

To fulfil this requirement I did exactly the same as in the previous task: I consulted the available documentation in the gradle website.
I found out that there is also a task type made exactly for this use case, the Zip task type.
With these 2 properties:

* archiveFileName: *The archive name.*
* destinationDirectory: *The directory where the archive will be placed.*

And this 1 method:

* from(sourcePaths): *Specifies source files or directories for a copy.*

The Zipper task was built, like this:

> task zipBackup(type: Zip){  
> &nbsp;&nbsp;&nbsp;&nbsp; archiveFileName = 'backup-0-1-2.zip'  
> &nbsp;&nbsp;&nbsp;&nbsp; destinationDirectory = file('backup/ziped')  
> &nbsp;&nbsp;&nbsp;&nbsp; from 'src'  
> }

Afterwards I just finished up this report and pushed it to the remote.

