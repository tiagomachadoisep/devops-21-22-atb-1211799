# Class Assignment 1 - Version Control with Git

## Part 1

## 1.1 Analysis, Design and Implementation

### Requirements:

* #### Copy the code of the Tutorial React.js and Spring Data REST Application into a new folder named CA1

This is a very basic task that can be accomplished either by using the OS GUI or using the terminal, with the command (
in windows):
> move "<source_directory>" "<destination_directory>"

* #### Commit the changes (and push them)

Before committing we need to add or remove the change in the working directory to the staging area. If we want to add
all the changes made to the staging are we use the following command:

> git add .

or

> git add *

If we don't want to add all the changes to the staging area we need to specify the files we want to add. If I wanted to
add the changes made in this file to the staging are I would use the following command:

> git add CA1/README.md

The commands used to remove files and add these changes to the staging are area are very similar, but instead of add we
use *rm*, like so:

> git rm CA1/README.md

A commit and a push can be done via the IDE, a Git GUI (like Sourcetree) or the command line, which is what we will be
using in this assignment. The first commit of this assignment was done with the following command:

> git commit -m "Create CA1 folder and move code from root to CA1, resolve issue #1"

Where the *-m* is followed by a message describing what was done on the commit enclosed on double quotes.

* #### Tag the initial version as v1.1.0. Push it to the server.

  Git supports two types of tags: *lightweight* and *annotated*. If we do not provide any of the *-a*, *-s*, or *-m*
  options, the tag will be of the lightweight type. The tags used in this assignment are all annotated, using the *-a*
  option. We can also pair messages with the tags using the *-m* option just like we would in a commit command. The
  first tag was added with the following command:

  > git tag -a v1.1.0 -m "version 1.1.0"

* #### Let's develop a new feature to add a new field to the application. In this case, let's add a new field to record the years of the employee in the company

  Adding support for this new field was fairly simple, we just needed to add the *jobYears* attribute to the Employee
  Class:

  > private String jobYears

  Add the new parameter to the constructor and the getter's and setter's method. After that I also needed to update the
  front-end of the application. To do that I added a new *table header* and a new *table data* to the *app.js* file
  using the *\<th>* and the *\<td>* tags.

  The app was tested using unit tests, and was debugged using the *React developer tools* for the front-end and for the
  back-end I checked that the app would not run if I tried to load some invalid data, like a negative year in the *
  jobYears* field.

  Several commits were made during this process, and I added a new tag running the following command:

  > git tag -a v1.2.0 -m "version 1.2.0"

* #### At the end of the assignment mark your repository with the tag ca1-part1

  The final requirement will be accomplished using the same commands I used before to add a tag:

  > git tag -a ca1-part1 -m "ca1-part1"

  This tag was only added when this report was complete.

## Part 2

## 2.1 Analysis, Design and Implementation

### Requirements:

* #### Develop new features in branches named after the feature. Create a branch named "email-field" to add a new email field to the application

In order to create a new branch named *email-field* I ran the following command:

> git checkout -b email-field

The *-b* option allows us to create a new branch, it's equivalent to:

> git branch email-field
>
> git checkout email-field)

After creating the branch I added a new field to the app, named *email*, running the same steps as before in Part 1.

When this new feature was done and fully tested I needed to merge the previously created branch with the master branch.
This was achieved with the following commands:

> git checkout master
>
> git merge email-field
>
> git push

Then to add a new tag named *v1.3.0* we did the same as in part 1, but this time I indicated the commit I wanted to tag,
like this:

> git tag -a v1.3.0 -m "v1.3.0" e1049ff271573f650df518d11585b50d5180f817

* #### Create branches for fixing bugs

For this task we created a new branch named *fix-invalid-email* like in previous requirement:

> git checkout -b fix-invalid-email

And after fixing the bug I just added all the changes made to the staging area, committed them and pushed.

I debugged the app end-to-end using the same tools as before.

The last steps were very simple, since I only needed to merge the branch as before and add some tags.

## 2.2 Analysis of an Alternative

There are two types of version control: centralized and distributed.

**Centralized version control**

With centralized version control systems, like Subversion (SVN) or Perforce, we have a single “central” copy of our
project on a server and commit our changes to this central copy. We pull the files that we need, but we never have a
full copy of our project locally, like in a distributed version control.

**Distributed version control**

With distributed version control systems (DVCS), we don't rely on a central server to store all the versions of a
project’s files. Instead, we clone a copy of a repository locally so that we have the full history of the project. Two
common distributed version control systems are Git and Mercurial.

In this assignment we will be experimenting with Mercurial.

## 3. Implementation of the Alternative

After downloading Mercury I initialized a repository with the following commands:

> cd /path/to/new/folder
> hg init
> echo "# Hello World" >> README.md
> hg add .
> hg commit -m "Initial commit"

When I tried to commit I got an error, pointing that I hadn't supplied a username. So i ran this command to edit the
configurations file:

> hg config --edit

This opened the mercurial.ini file where I changed the username line, which was empty and added an email line.

After that I tried to commit again, and it worked.

Finally, I pushed the changes to the remote repository with a push command containing the remote repository location:

> hg push https://1211799isepipppt@helixteamhub.cloud/miniature-bike-2256/projects/mercurialtest/repositories/mercurial/CA1

Now I wanted to add a tag to a commit, so I added some changes to the local repository, added them, committed them and
then Ran this command:

> hg tag "Second commit"

This created a new file in the repository named *.hgtags* which contains the commit identifier and the tag name.

Creating a new branch was pretty simple, I just ran the following command:

> hg branch branch-1

Now I just needed to merge this new branch.

First I updated both branches:

> hg update branch-1
> hg update default

And then to merge both:

> hg merge branch-1

This for some reason failed, outputting this error message:

> abort: merging with a working directory ancestor has no effect

After some troubleshooting I found out that I needed to make some changes to the default branch, so I switched to the
default branch and added a dummy file, committed the file and pushed. To checkout the default branch I just ran this
command:

> hg checkout default

Now, in my second try I ran:

> hg merge branch1-1

And I got no error message! So afterwards I just added everything, committed the changes and pushed them. Everything
worked as expected.

<img src="CA1/tut-react-and-spring-data-rest/repository.png">