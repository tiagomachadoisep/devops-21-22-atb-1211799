# Class Assignment 4 - Containers with Docker

## Part 2

## 1.1 Analysis, Design and Implementation

### Requirements:

* #### You should produce a solution similar to the one of the part 2 of the previous CA but now using Docker instead of Vagrant

To start this assignment I downloaded the repository shared and indicated by the teacher, [this one](https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/).
There were only 3 things I needed to 
change:
* Replace the repository that would be cloned by the web app, to my own;
* Refactor the *WORKDIR* instruction to match my app path;
* Refactor the command that copies the jar file to my own file's name;

* #### You should use docker-compose to produce 2 services/containers

To accomplish this task I typed:

> docker-compose build

And my first problem emerged:
![](img/first_error.png)

I figured out it was some compatibility issue with the jdk versions. So I changed the base image jdk version from 8 to 11, and it fixed it:
![](img/error1_fixed.png)

So after I tried to run the next command:

> docker-compose up

And It worked:
![](img/success.png)

* #### Publish the images (db and web) to Docker Hub (https://hub.docker.com)

For this task I repeated my steps of part1 doing:

> docker login -u tiago1211799
> docker tag compose_web:latest tiago1211799/test_repo:web_v1
> docker tag compose_db:latest tiago1211799/test_repo:db_v1
> docker push tiago1211799/test_repo:web_v1
> docker push tiago1211799/test_repo:db_v1

![](img/dockerhub.png)

* #### Use a volume with the db container to get a copy of the database file by using the exec to run a shell in the container and copying the database file to the volume.

For this task I first needed the id of the db service, so I typed:

> docker ps -a

Knowing the id, I typed:

> docker exec -it 31b8c2be0fbf bash

This game access to the service terminal, so now I just needed to copy the file to the volume, like so:

> cp ./jpadb.mv.db /usr/src/data

## 2 Analysis of an Alternative - Kubernetes

From my reading on this subject, comparing docker to kubernetes is like comparing  apples to apple pie, they are different technologies and work well together for building, developing and scaling containerized applications.

For short, kubernetes is a tool for managing and automating containerized workloads in the cloud.
Robinhood is an app used by millions of individuals to trade securities, and therefore, when the markets are close, it doesn't handle lots of trades, but, when the markets are open, it needs to fulfill millions of trades. Kubernetes is the tool that manages the infrastructure to handle the changing workload. Kubernetes can scale containers across multiple machines, and if one fails it knows how to replace it with a new one.

A system deployed on kubernetes is known as a cluster. The *brain* of the operation is known as the **control plane**, it exposes an api server that can handle both internal and external requests to manage the cluster it also contains its own key value database called **ETCD** used to store important information about the cluster. The control panel manages the **nodes**, which may be a virtual or physical machine, depending on the cluster. Each node runs something called **kubelet**, which is a tiny application that runs on the machine to communicate back with the control panel.

Inside each node we have multiple **pods** which is the smallest deployable unit in kubernetes. A pod is a set of containers running together. When the workload increases kubernetes can automatically scale horizontally by adding more nodes to the cluster. Kubernetes also handles networking, secret management, persistent storage and so on. Kubernetes also maintains a replica set which is a set of running pods or containers ready to go at any given time.

As a developer we define objects in yaml that describe the desired state of our cluster. For example, we might have a nginx deployment that has a replica set of 3 pods. In the spec field we can define exactly how it should behave, like its containers, ports, volumes and so on. We use this configuration to provision and scale containers automatically and ensure that they are always up and running.


## 3. Implementation of the Alternative (theoretical)

The approach that I chose to follow, involves using **Kompose** to translate a Docker Compose file to Kubernetes resources.
First of, there are some requirements:
* We need a kubernetes cluster;
* The kubectl command-line tool must be configured to communicate with the cluster;

For starters, we would need to install Kompose, since I am using Ubuntu, I would need this command:

> wget https://github.com/kubernetes/kompose/releases/download/v1.26.1/kompose_1.26.1_amd64.deb # Replace 1.26.1 with latest tag
> sudo apt install ./kompose_1.26.1_amd64.deb

For others operating systems, there are installation guides in the Kompose [website](https://kompose.io/installation/).

Afterwards we need to change our working directory to the directory that has the docker-compose.yml that we want to convert. Having done that, all we need is to run the following command:

> kompose convert

Next, to deploy our app, we just run:

> kompose up

 

