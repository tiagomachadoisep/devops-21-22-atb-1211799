# Class Assignment 4 - Containers with Docker

## Part 1

## 1.1 Analysis, Design and Implementation

### Requirements:

* #### You should create a docker image (i.e., by using a Dockerfile) to execute the chat server

To start this assignment I just created a file and named it *Dockerfile*. 
The *FROM* instruction initializes a new build stage and sets the base image for subsequent instructions, in this case, the base image used was ubuntu, since I found it to be the norm.

Next we handle the dependencies, such as git and java.

Afterwards with the *WORKDIR* instruction we set our working directory. By default, if the working directory does not exist, it will be created.

Next I cloned my individual repository, and changed the working directory.

The *RUN* instruction executes commands in the container terminal. Using this instruction I gave gradle wrapper execute permission, and built the project.

The *EXPOSE* instruction informs Docker that the container listens on the specified network ports at runtime. This instruction functions like a documentation, and we document that we intend to use the port 59001.

The main purpose of a *CMD* is to provide defaults for an executing container. In our case we executed the app with the following command:

> java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

To build our image based in our docker file I ran the following command in the directory of the docker file:

> docker build . -t chat_v1

To start our container I ran this command:

> docker run -p 59001:59001 -d chat_v1

Having done this I could easily run the task to bring up the chat GUI:

![](img/1.png)

* #### You should tag the image and publish it in docker hub

To start we need to create an account and activate it in the docker hub. Next we need to create our repository in the docker hub.

Next up we need to log in with our username in the terminal:

> docker login -u [username]

After logging in we need to tag our image and push it to our repository.

To tag the image I used the following command:

> docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]

In my case it was:

> docker tag chat_v1:latest tiago1211799/test_repo:chat_v1

And to push:

> docker push TARGET_IMAGE[:TAG]

In my case it was:

> docker push tiago 1211799/test_repo:chat_v1

![](img/3.png)
* ####  To explore the concept of docker images you should create two versions of your solution

For this other version the docker file needed was much simpler. 
Our base image is the openJDK version 11, and afterwards we need copy the executable to the container using the *COPY* instruction. With the *CMD* instruction we can command the container to execute the jar file.
![](img/2.png)
